public class date {
    int year;
    int month;
    int day;
    int hour;
    int minute;
    int second;
    public date(int year,int month,int day,int hour,int minute,int second){

    }
    public int readdate(int year,int month,int day,int hour,int minute,int second) {
        if (day > 30)
        {
            this.day=day-30;
            setDay(this.day);
            month++;
            setMonth(month);
        }
        if (month > 12)
        {
            this.month=month-12;
            setMonth(this.month);
            year++;
            setYear(year);
        }
        if(minute>60)
        {
            this.minute=minute-60;
            setMinute(this.minute);
            hour++;
            setHour(hour);
        }
        if(second>60)
        {
            this.second=second-60;
            setSecond(this.second);
            minute++;
            setMinute(minute);
        }
        return 0;

    }


    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }
}
