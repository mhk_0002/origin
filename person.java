import java.util.Scanner;

public class person {
    int id;
    String firstname;
    String lastname;
    int newid;
    String newfirstname;
    String newlastname;
    public void readperson(){
        Scanner input=new Scanner(System.in);
        id=input.nextInt();
        firstname=input.next();
        lastname=input.next();
        setId(id);
        setFirstname(firstname);
        setLastname(lastname);
    }
    public void editid(int newid) {
        setId(newid);
    }
    public void editfirstname(String newfirstname) {
        setFirstname(newfirstname);
    }
    public void editlastname(String newlastname) {
        setLastname(newlastname);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getNewid() {

        return newid;
    }

    public void setNewid(int newid)
    {
        this.newid = newid;
    }

    public String getNewfirstname() {

        return newfirstname;
    }

    public void setNewfirstname(String newfirstname) {

        this.newfirstname = newfirstname;
    }

    public String getNewlastname() {

        return newlastname;
    }

    public void setNewlastname(String newlastname) {

        this.newlastname = newlastname;
    }
}
